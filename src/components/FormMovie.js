import React, { useState } from 'react';
import MyButton from './MyButton';

function FormMovie(props) {
    const { saveMovie } = props;

    const [ title, setTitle ] = useState('');

    const save = (e) => {
        e.preventDefault(); // function ini mencegah page ke-refresh
        saveMovie(title); // memanggil function dari parent
        setTitle('');
    }

    return (
        <React.Fragment>
            <form>
                <input type="text" value={title} placeholder="place input movie title" onChange={(e) => setTitle(e.target.value)}/> 
                {/* <button type="submit" onClick={save}>Save</button> */}
                <MyButton title="Save" funcClick={save}/>
            </form>
        </React.Fragment>
    )
}

export default FormMovie;