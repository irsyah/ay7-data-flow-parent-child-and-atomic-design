import MyButton from "./MyButton";

function Movie(props) {

    const { movie, deleteMovie } = props;
    const removeMovie = () => {
        console.log('delete movie');
        deleteMovie(movie.id);
    }

    return (
        <div>
            <h3>{movie.title}</h3>
            {/* <button onClick={removeMovie}>Delete</button> */}
            <MyButton title="Delete" funcClick={removeMovie}/>
        </div>
    )
}

export default Movie